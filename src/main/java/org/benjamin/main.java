package org.benjamin;

import me.mrCookieSlime.Slimefun.Lists.RecipeType;
import me.mrCookieSlime.Slimefun.Lists.SlimefunItems;
import me.mrCookieSlime.Slimefun.Objects.Category;
import me.mrCookieSlime.Slimefun.Objects.SlimefunItem.SlimefunItem;
import me.mrCookieSlime.Slimefun.cscorelib2.item.CustomItem;
import org.benjamin.listener.events;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public final class main extends JavaPlugin {

    public static ItemStack heartT1;
    public static ItemStack heartT2;
    public static ItemStack heartT3;
    public static ItemStack heartCanT1;
    public static ItemStack heartCanT2;
    public static ItemStack heartCanT3;
    public static HashMap<Player, Integer> T1 = new HashMap<>();
    public static HashMap<Player, Integer> T2 = new HashMap<>();
    public static HashMap<Player, Integer> T3 = new HashMap<>();

    @Override
    public void onEnable() {
        // Plugin startup logic

        RecipeType drop = new RecipeType(new CustomItem(Material.DIAMOND_SWORD, "&bMob Drop", "&fKill The Specified Mob/s"));

        // Listener
        this.getServer().getPluginManager().registerEvents(new events(), this);

        // Create Hearts O' Plenty Category
        ItemStack hopItem = new CustomItem(Material.RED_DYE, "&7Hearts O' Plenty", "", "&a> Click to open");
        Category hopCat = new Category(hopItem);

        // Create Hearts O' Plenty Items
        heartT1 = new CustomItem(Material.RED_DYE, "&cRed Heart", "", "&aLevel I", "", "&fRestores 2 Hearts", "", "&eRight click &7to consume");
        heartT2 = new CustomItem(Material.YELLOW_DYE, "&eYellow Heart", "", "&aLevel II", "", "&fRestores 4 Hearts", "", "&eRight click &7to consume");
        heartT3 = new CustomItem(Material.BLUE_DYE, "&bBlue Heart", "", "&aLevel III", "", "&fRestores 6 Hearts", "", "&eRight click &7to consume");

        heartCanT1 = new CustomItem(Material.RED_DYE, "&cRed Heart Canister", "", "&aLevel I", "", "&fGrants the user 1 heart", "", "&fMax Amount: &710", "", "&eRight click &7to consume");
        heartCanT2 = new CustomItem(Material.YELLOW_DYE, "&eYellow Heart Canister", "", "&aLevel II", "", "&fGrants the user 1 heart", "", "&fMax Amount: &710", "", "&eRight click &7to consume");
        heartCanT3 = new CustomItem(Material.BLUE_DYE, "&bBlue Heart Canister", "", "&aLevel III", "", "&fGrants the user 1 heart", "", "&fMax Amount: &710", "", "&eRight click &7to consume");

        ItemStack[] heartRecT1 = {
                null, null, null,
                null, new CustomItem(Material.TURTLE_SPAWN_EGG, "&7Zombie, Creeper, Spider", "", "&8Chance: &b10%"), null,
                null, null, null
        };

        ItemStack[] heartRecT2 = {
                null, null, null,
                null, new CustomItem(Material.TURTLE_SPAWN_EGG, "&7Blaze, Ghast, Wither Skeleton", "", "&8Chance: &b5%"), null,
                null, null, null
        };

        ItemStack[] heartRecT3 = {
                null, null, null,
                null, new CustomItem(Material.TURTLE_SPAWN_EGG, "&7Ender Dragon, Wither", "", "&8Chance: &b100%"), null,
                null, null, null
        };

        ItemStack[] heartCanT1Rec = {
                SlimefunItems.CAN, heartT1, SlimefunItems.BLANK_RUNE,
                SlimefunItems.ESSENCE_OF_AFTERLIFE, SlimefunItems.ENDER_LUMP_1, null,
                null, null, null
        };
        ItemStack[] heartCanT2Rec = {
                heartCanT1, heartT2, SlimefunItems.RUNE_ENDER,
                SlimefunItems.ENDER_LUMP_2, null, null,
                null, null, null
        };
        ItemStack[] heartCanT3Rec = {
                heartCanT2, heartT3, SlimefunItems.RUNE_RAINBOW,
                SlimefunItems.ENDER_LUMP_3, null, null,
                null, null, null
        };

        // Setup registry for items
        SlimefunItem heartT1Reg = new SlimefunItem(hopCat, heartT1, "HEART_1", drop, heartRecT1);
        SlimefunItem heartT2Reg = new SlimefunItem(hopCat, heartT2, "HEART_2", drop, heartRecT2);
        SlimefunItem heartT3Reg = new SlimefunItem(hopCat, heartT3, "HEART_3", drop, heartRecT3);

        SlimefunItem heartCanT1Reg = new SlimefunItem(hopCat, heartCanT1, "HEART_CAN_1", RecipeType.MAGIC_WORKBENCH, heartCanT1Rec);
        SlimefunItem heartCanT2Reg = new SlimefunItem(hopCat, heartCanT2, "HEART_CAN_2", RecipeType.MAGIC_WORKBENCH, heartCanT2Rec);
        SlimefunItem heartCanT3Reg = new SlimefunItem(hopCat, heartCanT3, "HEART_CAN_3", RecipeType.MAGIC_WORKBENCH, heartCanT3Rec);

        //Register Hearts O' Plenty Items
        heartT1Reg.register();
        heartCanT1Reg.register();
        heartT2Reg.register();
        heartCanT2Reg.register();
        heartT3Reg.register();
        heartCanT3Reg.register();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
