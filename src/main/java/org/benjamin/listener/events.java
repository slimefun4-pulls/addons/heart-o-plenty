package org.benjamin.listener;

import me.mrCookieSlime.Slimefun.Setup.SlimefunManager;
import org.benjamin.main;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

public class events implements Listener {
    @SuppressWarnings("deprecation")
    @EventHandler
    public void entityInteract(PlayerInteractAtEntityEvent event) {
        Player player = event.getPlayer();
        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }
        if (SlimefunManager.isItemSimiliar(player.getItemInHand(), main.heartT1, true)) {
            event.setCancelled(true);
            return;
        } else if (SlimefunManager.isItemSimiliar(player.getItemInHand(), main.heartT2, true)) {
            event.setCancelled(true);
            return;
        } else if (SlimefunManager.isItemSimiliar(player.getItemInHand(), main.heartT3, true)) {
            event.setCancelled(true);
            return;
        } else if (SlimefunManager.isItemSimiliar(player.getItemInHand(), main.heartCanT1, true)) {
            event.setCancelled(true);
            return;
        } else if (SlimefunManager.isItemSimiliar(player.getItemInHand(), main.heartCanT2, true)) {
            event.setCancelled(true);
            return;
        } else if (SlimefunManager.isItemSimiliar(player.getItemInHand(), main.heartCanT3, true)) {
            event.setCancelled(true);
            return;
        }
    }

    @EventHandler
    @SuppressWarnings("deprecation")
    public void onRightClick(PlayerInteractEvent event) {
        if (event.getAction() == Action.PHYSICAL || event.getHand() != EquipmentSlot.HAND) {
            return;
        }
        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            Player player = event.getPlayer();
            if (SlimefunManager.isItemSimiliar(player.getItemInHand(), main.heartT1, true)) {
                event.setCancelled(true);
                if (player.getHealth() != player.getMaxHealth()) {
                    if (player.getHealth() + 2 >= player.getMaxHealth()) {
                        player.setHealth(player.getMaxHealth());
                    } else {
                        player.setHealth(player.getHealth() + 4);
                    }
                    player.getInventory().getItemInHand().setAmount(player.getInventory().getItemInHand().getAmount() - 1);
                }
            }
            if (SlimefunManager.isItemSimiliar(player.getItemInHand(), main.heartT2, true)) {
                event.setCancelled(true);
                if (player.getHealth() != player.getMaxHealth()) {
                    if (player.getHealth() + 2 >= player.getMaxHealth()) {
                        player.setHealth(player.getMaxHealth());
                    } else {
                        player.setHealth(player.getHealth() + 8);
                    }
                    player.getInventory().getItemInHand().setAmount(player.getInventory().getItemInHand().getAmount() - 1);
                }
            }
            if (SlimefunManager.isItemSimiliar(player.getItemInHand(), main.heartT3, true)) {
                event.setCancelled(true);
                if (player.getHealth() != player.getMaxHealth()) {
                    if (player.getHealth() + 2 >= player.getMaxHealth()) {
                        player.setHealth(player.getMaxHealth());
                    } else {
                        player.setHealth(player.getHealth() + 12);
                    }
                    player.getInventory().getItemInHand().setAmount(player.getInventory().getItemInHand().getAmount() - 1);
                }
            }
            if (SlimefunManager.isItemSimiliar(player.getItemInHand(), main.heartCanT1, true)) {
                event.setCancelled(true);
                main.T1.putIfAbsent(player, 0);
                int T1 = main.T1.get(player);
                if (T1 + 1 <= 10) {
                    player.setMaxHealth(player.getMaxHealth() + 2);
                    player.getInventory().getItemInHand().setAmount(player.getInventory().getItemInHand().getAmount() - 1);
                    main.T1.put(player, T1 + 1);
                    return;
                } else {
                    return;
                }
            }
            if (SlimefunManager.isItemSimiliar(player.getItemInHand(), main.heartCanT2, true)) {
                event.setCancelled(true);
                main.T2.putIfAbsent(player, 0);
                int T2 = main.T2.get(player);
                if (T2 + 1 <= 10) {
                    player.setMaxHealth(player.getMaxHealth() + 2);
                    player.getInventory().getItemInHand().setAmount(player.getInventory().getItemInHand().getAmount() - 1);
                    main.T2.put(player, T2 + 1);
                    return;
                } else {
                    return;
                }
            }
            if (SlimefunManager.isItemSimiliar(player.getItemInHand(), main.heartCanT3, true)) {
                event.setCancelled(true);
                main.T3.putIfAbsent(player, 0);
                int T3 = main.T3.get(player);
                if (T3 + 1 <= 10) {
                    player.setMaxHealth(player.getMaxHealth() + 2);
                    player.getInventory().getItemInHand().setAmount(player.getInventory().getItemInHand().getAmount() - 1);
                    main.T3.put(player, T3 + 1);
                }
            }
        }
    }

    @EventHandler
    public void mobSlain(EntityDeathEvent event) {
        LivingEntity entity = event.getEntity();
        double randDouble = Math.random();
        if (entity instanceof Zombie || entity instanceof Spider || entity instanceof Creeper) {
            if (randDouble <= 0.10D) {
                entity.getLocation().getWorld().dropItem(entity.getLocation(), main.heartT1);
            }
        } else if (entity instanceof Blaze || entity instanceof Ghast || entity instanceof WitherSkeleton) {
            if (randDouble <= 0.05D) {
                entity.getLocation().getWorld().dropItem(entity.getLocation(), main.heartT2);
            }
        } else if (entity instanceof EnderDragon || entity instanceof Wither) {
            entity.getLocation().getWorld().dropItem(entity.getLocation(), main.heartT3);
        }
    }
}
